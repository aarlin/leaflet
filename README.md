Copy from https://github.com/name5566/leaf 

Leaf
====
A pragmatic game server framework in Go (golang).

Features
---------

* Extremely easy to use
* Reliable
* Multicore support
* Modularity

Community
---------

* QQ 群：376389675

Documentation
---------

* [中文文档](https://gitee.com/aarlin/leaflet/blob/master/TUTORIAL_ZH.md)
* [English](https://gitee.com/aarlin/leaflet/blob/master/TUTORIAL_EN.md)

Licensing
---------

Leaf is licensed under the Apache License, Version 2.0. See [LICENSE](https://gitee.com/aarlin/leaflet/blob/master/LICENSE) for the full license text.

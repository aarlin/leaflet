module gitee.com/aarlin/leaflet

go 1.15

require (
	github.com/golang/protobuf v1.4.3
	github.com/gorilla/websocket v1.4.2
	go.mongodb.org/mongo-driver v1.4.5
)
